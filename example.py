# coding: utf-8
from flask import Flask, render_template, request
from flask_googlemaps import GoogleMaps
from flask_googlemaps import Map, icons
import json
from spojene_moje_matove import get_rank,get_data
import asyncio
import random

#from dynaconf import FlaskDynaconf

app = Flask(__name__, template_folder="templates")
#FlaskDynaconf(app)  # will read GOOGLEMAPS_KEY from .secrets.toml
rank =0
missing =""


# you can set key as config
# app.config['GOOGLEMAPS_KEY'] = "AIzaSyDP0GX-Wsui9TSDxtFNj2XuKrh7JBTPCnU"

# you can also pass key here
GoogleMaps(
    app,
    key="AIzaSyA5IIhOFqZKE60mFSQ-MVhf3M7KxBx2-Bo"
)

# NOTE: this example is using a form to get the apikey
# @app.route("/data/",methods=["POST","GET"])
# def return_data():
#     return(render_template)

@app.route("/",methods=["POST","GET"])
def fullmap():
    if request.method == 'POST' and len(request.data)>10:
        loop=asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        print(request.data)
        data = json.loads(request.data)

        print(data)
        print(data['latitude'])
        print(data['longtitude'])
        rankk,missingg =get_rank([21.214599609375, 48.719961222646276])
        # rankk,missingg=get_data()
        # task =loop.create_task(get_rank([21.214599609375, 48.719961222646276]))
        # loop.run_until_complete(task)
        asyncio.sleep(60)


        # print(task)

        print(data['health_facilities'])
        print('---------------------------------------------------------')
        # return "oka"
        # return render_template(
        # "example_fullmap.html",
        # fullmap=fullmap,
        # rank = rankk,
        # missing = missingg,
        # GOOGLEMAPS_KEY=request.args.get("apikey"),


    def handle_click(lat, lng):
        print("Clicked on map at latitude {}, longitude {}".format(lat, lng))
    fullmap = Map(
        identifier="fullmap",
        varname="fullmap",
        style=(
            "height:100%;"
            "width:70%;"
            "top:0;"
            "left:0;"
            "position:absolute;"
            "z-index:200;"
        ),
        lat=48.7164,
        lng=21.2611,
        markers=[
            {
                "icon": "//maps.google.com/mapfiles/ms/icons/green-dot.png",
                "lat": 48.7164,
                "lng": 21.2611,
                "infobox": "Hello I am <b style='color:green;'>GREEN</b>!",
            },
            {
                "icon": "//maps.google.com/mapfiles/ms/icons/blue-dot.png",
                "lat": 48.7164,
                "lng": 21.2611,
                "infobox": "Hello I am <b style='color:blue;'>BLUE</b>!",
            },
            {
                "icon": icons.dots.yellow,
                "title": "Click Here",
                "lat": 48.7164,
                "lng": 21.2611,
                "infobox": (
                    "Hello world!"
                    "<h2>ahoj svet</h2>"
                    # "<img src='//placehold.it/50' >"
                    # "<br>Images allowed!"
                ),
            },
        ],
        click_listener=[handle_click],
        # maptype = "TERRAIN",
        # zoom="5"
    )

    lat = request.args.get('')
    # categories = [
    #     {'name': 'Food', 'checked': False},
    #     {'name': 'Clothing', 'checked': False},
    #     {'name': 'Electronics', 'checked': False},
    #     {'name': 'Books', 'checked': False},
    #     {'name': 'Toys', 'checked': False},
    # ]
    return render_template(
        "example_fullmap.html",
        fullmap=fullmap,
        rank = random.randint(40,80),
        missing = missing,
        GOOGLEMAPS_KEY=request.args.get("apikey"),
    )

if __name__ == "__main__":
    app.run(port=5050, debug=True, use_reloader=True)