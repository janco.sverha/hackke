import gmaps
import googlemaps
from datetime import datetime
import re
from geopy.distance import geodesic as GD
from geopy import Point
import multiprocessing


list_to_return = []
def get_data():
    return list_to_return

with open('apikey.txt') as f:
    api_key = f.readline()
    f.close
now = datetime.now()
gmaps = googlemaps.Client(api_key)

def position_in_range(lat_user, lon_user, distance_in_km, lat_object, lon_object):
    origin = Point(lat_user, lon_user)
    destination0 = GD(kilometers=distance_in_km).destination(origin, 90)
    _, max_lon = destination0.latitude, destination0.longitude
    destination1 = GD(kilometers=distance_in_km).destination(origin, -90)
    _, min_lon = destination1.latitude, destination1.longitude
    destination2 = GD(kilometers=distance_in_km).destination(origin, 0)
    max_lat, _ = destination2.latitude, destination2.longitude
    destination3 = GD(kilometers=distance_in_km).destination(origin, 180)
    min_lat, _ = destination3.latitude, destination3.longitude
    if lat_object >= min_lat and lat_object <= max_lat:
        if lon_object >= min_lon and lon_object <= max_lon:
            return True
    return False


def process_line(line, shared_list,lonlan):
    line = line.replace(",,",", ,")
    vals = re.findall(r'[^",]+|"[^"]+"', line)[:10]
    if(vals[0] != "name"):

        if(position_in_range(lonlan[0],lonlan[1],1.5,float(vals[5]),float(vals[4]))):
            results = gmaps.directions(lonlan,(vals[5],vals[4]),mode='walking')
            distance =results[0]['legs'][0]['distance']['text']
            words = results[0]['legs'][0]['duration']['text'].split()
            if "hours" in words or "hour" in words:
                hours_in_minutes = int(words[0]) * 60
                mins = int(words[2])
                total_minutes = hours_in_minutes + mins
            else:
                total_minutes = int(words[0])
            if(total_minutes<15):
                shared_list.append([vals[3],total_minutes,distance,True,False,False])

# top_left_lat =48.7622
# top_left_lon = 21.1997
# top_right_lat = 48.7622
# top_right_lon = 21.3170
# bottom_left_lat = 48.6812
# bottom_left_lon = 21.1997
# bottom_right_lat = 48.6812
# bottom_right_lon = 21.3170
# x = np.linspace(top_left_lon, top_right_lon, 10)
# y = np.linspace(bottom_right_lat, top_right_lat, 10)
# grid = [[lon,lan] for lon in x for lan in y]

def get_rank(lonlan,preferences=""):
    if(preferences == ""):
        preferences = {
        'health_facilities': 5,
        'convenience_facilities': 5,
        'entertainment_facilities': 5,
        'restaurant_facilities': 5,
        'educational_facilities': 5,
        'dog_facilities': 5,
        'transport_facilities': 5,
        'post_facilities': 5,
        'sport_facilities': 5,
        'walk_accessibility': 5,
        'bike_accessibility': 5 ,
        'bus_accessibility': 5,
        }

    weights = preferences.copy()

    health_facilities = ['chemist', 'Všeobecná ambulancia pre deti', 'ambulancia zubného lekárstva', 'pharmacy', 'Všeobecná ambulancia pre dospelých']
    convenience_facilities = ['convenience','supermarket','chemist']
    entertainment_facilities = ['pub','playground','bar']
    restaurant_facilities = ['cafe','restaurant','fast_food']
    educational_facilities = ['ZS_cirkevná','ZS_štátna','MS_štátna','MS_štátne','MS_súkromná','MS_cirkevná','ZS_súkromná']
    dog_facilities = ['Vybehy_psy']
    transport_facilities = ['MHD']
    post_facilities = ['parcel_locker','Pošty']
    sport_facilities = ['fitness_station','fitness_fitness']
    categories = {
        'health_facilities': health_facilities,
        'convenience_facilities': convenience_facilities,
        'entertainment_facilities': entertainment_facilities,
        'restaurant_facilities': restaurant_facilities,
        'educational_facilities': educational_facilities,
        'dog_facilities': dog_facilities,
        'transport_facilities': transport_facilities,
        'post_facilities': post_facilities,
        'sport_facilities': sport_facilities
    }
    counts = {category: 0 for category in categories}
    rank = 0



    with open("POIs_location_catchments.csv") as f:
        lines = [line.rstrip() for line in f]

    with multiprocessing.Manager() as manager:
        shared_list = manager.list()

        with multiprocessing.Pool(processes= multiprocessing.cpu_count()-4) as pool:
            pool.starmap(process_line, ((line, shared_list,[lonlan[1],lonlan[0]]) for line in lines))

        for row in shared_list:
                for category, facilities in categories.items():
                    if row[0] in facilities:
                        counts[category]+=1
                        rank += weights[category]
                        weights[category]/=2
    missing_categories = []
    for category, count in counts.items():
        if count == 0:
            missing_categories.append(category)

        # print(f'{category}: {count}')
    missing_facilities = list()
    if len(missing_categories) >0:
        # print('This location should focus on services in these categories:')
        for missing_category in missing_categories:
            # print(missing_category)
            missing_facilities.append(missing_category)
    list_to_return = [rank,missing_facilities]
    # refresh()
    return rank,missing_facilities